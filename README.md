# Arachnid Testing

## Overview

Arachnid Testing provides utilities and verification code for writing legible tests for Arachnid components. Simple syntax to verify that the result of a Arachnid computation is as expected is provided to enable the creation of simple unit tests for complex functionality.

## Status

[![pipeline status](https://gitlab.com/arachnid-project/arachnid-testing/badges/master/pipeline.svg)](https://gitlab.com/arachnid-project/arachnid-testing/commits/master)

## See Also

For more information see the [meta-repository for the Arachnid Web Stack](https://gitlab.com/arachnid-project/arachnid), along with the main [arachnid.io](https://arachnid.io) site.
